package ostapchuk;

import ostapchuk.controller.Controller;
import ostapchuk.controller.CurrentController;
import ostapchuk.model.ModelStarter;

/**
 * DONE Create your own annotation.
 * DONE Create class with a few fields, some of which annotate with this annotation.
 *
 * DONE Through reflection print those fields in the class that were annotated by this annotation.
 *
 * TDONE Print annotation value into console (e.g. @Annotation(name = "111"))
 *
 * DOME Invoke methods (three methods with different parameters and return types)
 *
 * Set value into field not knowing its type.
 *
 * DONE  Invoke myMethod(String a, int ... args) and myMethod(String … args).
 *
 * Create your own class that receives object of unknown type and shows all information about that Class.
 */

public class Main {
    public static void main(String[] args) {
        Controller controller = new CurrentController();
        controller.start(new ModelStarter());
    }
}
