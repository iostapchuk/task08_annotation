package ostapchuk.controller;

import ostapchuk.model.Model;

public interface Controller {
    void start(Model receivedModel);
}
