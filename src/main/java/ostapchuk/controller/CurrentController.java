package ostapchuk.controller;

import ostapchuk.model.Model;

public class CurrentController implements Controller {
    public void start(Model receivedModel) {
        receivedModel.start();
    }
}
