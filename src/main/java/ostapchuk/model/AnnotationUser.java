package ostapchuk.model;

public class AnnotationUser {
    @MyAnnotation int firstInt = 3;
    @MyAnnotation String date = "03.05.19";
    double piNumber = 3.14;
    @MyAnnotation double eulersNumber = 2.71;
    @MyAnnotation SomeClass myClass = new SomeClass();

    public int firstMethod(int index) {
        return index + index + index;
    }

    String secondMethod(Object object) {
        return "Object to string: " + object.toString();
    }

    private double thirdMethod(String... strings) {
        String allStrings = "";
        for (String i : strings) {
            allStrings += i;
        }
        return allStrings.length();
    }

    String fourthMethod(String exampleString, int ... args) {
        return "First: " + exampleString + args.length + " and " + args[0];
    }

    String myMethod(String exampleString, int ... arguments) {
        return "String and int varargs";
    }

    String myMethod(String  ... arguments) {
        return "String varargs";
    }
}
