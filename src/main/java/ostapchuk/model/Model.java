package ostapchuk.model;

public interface Model {
    void start();
    void workWithAnnotations();
}
