package ostapchuk.model;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import ostapchuk.view.ConsoleView;
import ostapchuk.view.View;

public class ModelStarter implements Model {
    private View view;
    private static final String FIELD_VALUE = "Value of this field: ";

    public void start() {
        view = new ConsoleView();
        workWithAnnotations();
    }

    public void workWithAnnotations() {
        Class annotationUserClass = AnnotationUser.class;
        getFields(annotationUserClass);
        printAnnotationValue(annotationUserClass);
        workWithMethods(annotationUserClass);
    }

    private void workWithMethods(Class currentClass) {
        Method[] methods = currentClass.getDeclaredMethods();
        setMethodsAccessible(methods);
        invokeMethods(methods);
    }

    private void invokeMethods(Method[] methods) {
        // example object
        Object exampleObject = new Object();
        // just example String arguments
        String[] arguments = {"thy", "will", "be", "done"};
        // example integer arguments
        int[] argumentIntegers = {8, 3, 42, -4};
        try {
            view.printMe("Invoking methods...");
            for (Method method : methods) {
                view.printMe("invoking " + method.getName());
                if (method.getName().equals("firstMethod")) {
                    view.printMe(argumentIntegers[0] + " * 3 = "
                            + method.invoke(new AnnotationUser(),
                            new Integer(argumentIntegers[0])).toString());
                } else if (method.getName().equals("secondMethod")) {
                    view.printMe((String) method.invoke(
                            new AnnotationUser(), exampleObject));
                } else if (method.getName().equals("thirdMethod")) {
                    view.printMe("length " + method.invoke(new AnnotationUser(),
                            (Object) arguments).toString());
                } else if (method.getName().equals("fourthMethod")) {
                    view.printMe(method.invoke(new AnnotationUser(),
                            "lo-fi hip-hop", argumentIntegers).toString());
                } else if (method.getName().equals("myMethod")) {
                    if (method.getParameterCount() == 1) {
                        view.printMe((String) method.invoke(
                                new AnnotationUser(), (Object) arguments));
                    } else {
                        view.printMe((String) method.invoke(new AnnotationUser(),
                                arguments[0], argumentIntegers));
                    }
                }
            }
        } catch (InvocationTargetException | IllegalAccessException e) {
            view.printMe(e.getMessage());
        }
    }

    private void setMethodsAccessible(Method[] methods) {
        for (Method method : methods) {
            method.setAccessible(true);
        }
    }

    private void getFields(Class currentClass) {
        Field[] fields = currentClass.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(MyAnnotation.class)) {
                try {
                    printFieldsValue(field, new AnnotationUser());
                } catch (IllegalAccessException e) {
                    view.printMe(e.getMessage());
                }
            }
        }
    }

    private void printAnnotationValue(Class currentClass) {
        Annotation myAnnotation = currentClass.getDeclaredFields()[0]
                .getAnnotation(MyAnnotation.class);
        view.printMe("Value of MyAnnotation is: "
                + ((MyAnnotation) myAnnotation).value());
    }

    private void printFieldsValue(Field field, Object obj)
            throws IllegalAccessException {
        view.printMe("Name of this value is: " + field.getName());
        if ((field.getType().toString().equals("int"))) {
            view.printMe("This field is an int value.");
            view.printMe(FIELD_VALUE + field.getInt(obj));
        } else if (field.getType().toString().equals("double")) {
            view.printMe("This field is a double value.");
            view.printMe(FIELD_VALUE + field.getDouble(obj));
        } else if (field.getType().toString()
                .equals("class java.lang.String")) {
            view.printMe("This field is a String value.");
            view.printMe(FIELD_VALUE + field.get(obj));
        } else {
            view.printMe(field.toGenericString());
        }
        view.printMe(" ");
    }
}
