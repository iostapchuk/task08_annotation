package ostapchuk.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConsoleView implements View {
    private static final Logger LOGGER = LogManager.getLogger(ConsoleView.class);

    public void printMe(String s) {
        LOGGER.info(s);
    }
}
